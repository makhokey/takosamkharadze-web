import React, { Component } from "react";
import Helmet from "react-helmet";
import { injectIntl } from "gatsby-plugin-intl"
import Layout from "../components/Layout";
import About from "../components/About"
import config from "../../data/SiteConfig";
import SEO from "../components/SEO";

class AboutPage extends Component {
  render() {
    const {intl} = this.props;
    return (
      <Layout>
        <Helmet title={`${intl.formatMessage({ id: 'about.title' })} | ${config.siteTitle}`} />
        <SEO />
        <About />
      </Layout>
    );
  }
}

export default injectIntl(AboutPage);
