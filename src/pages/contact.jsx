import React, { Component } from "react";
import Helmet from "react-helmet";
import { injectIntl } from "gatsby-plugin-intl"
import Layout from "../components/Layout";
import Contact from "../components/Contact"
import config from "../../data/SiteConfig";
import SEO from "../components/SEO";

class ContactPage extends Component {
  render() {
    const {intl} = this.props;
    return (
      <Layout>
        <Helmet title={`${intl.formatMessage({ id: 'contact.title' })} | ${config.siteTitle}`} />
        <SEO />
        <Contact />
      </Layout>
    );
  }
}

export default injectIntl(ContactPage);
