import React, { Component } from "react";
import Helmet from "react-helmet";
import { injectIntl } from "gatsby-plugin-intl";
import Layout from "../components/Layout";
import config from "../../data/SiteConfig";
import SEO from "../components/SEO";

class ThanksPage extends Component {
  render() {
    const {intl} = this.props;
    return (
      <Layout>
        <Helmet title={`${intl.formatMessage({ id: 'thanks.title' })} | ${config.siteTitle}`} />
        <SEO />
        <section className="hero is-fullheight-with-navbar">
          <div className="hero-body">
            <div className="columns is-centered is-multiline">
              <div className="column is-12">
                <h1 className="title is-2 has-text-centered">
                  {intl.formatMessage({ id: 'thanks.desc' })}
                </h1>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    );
  }
}

export default injectIntl(ThanksPage);
