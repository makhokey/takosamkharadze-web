import React from "react";
import Helmet from "react-helmet";
import { graphql, Link } from "gatsby";
import Layout from "../components/Layout";
import PostListing from "../components/PostListing/PostListing";
import SEO from "../components/SEO/SEO";
import config from "../../data/SiteConfig";

class Listing extends React.Component {
  renderPaging() {
    const { currentPageNum, pageCount } = this.props.pageContext;
    const cat = '';
    const prevPage = currentPageNum - 1 === 1 ? "/" : `/${currentPageNum - 1}/`;
    const nextPage = `${cat}/${currentPageNum + 1}/`;
    const isFirstPage = currentPageNum === 1;
    const isLastPage = currentPageNum === pageCount;

    return (
      <nav className="pagination is-centered" role="navigation" aria-label="pagination">
        {!isFirstPage && <Link className="pagination-previous" to={cat+prevPage}>Previous</Link>}
        {[...Array(pageCount)].map((_val, index) => {
          const pageNum = index + 1;
          return (
            <Link
              className="pagination-link"
              key={`listing-page-${pageNum}`}
              to={pageNum === 1 ? cat : `${cat}/${pageNum}/`}
            >
              {pageNum}
            </Link>
          );
        })}
        {!isLastPage && <Link className="pagination-next" to={nextPage}>Next</Link>}
      </nav>
    );
  }

  render() {
    const postEdges = this.props.data.allMarkdownRemark.edges;

    return (
      <Layout>
        <Helmet title={config.siteTitle} />
        <SEO />
        <PostListing postEdges={postEdges} />
      </Layout>
    );
  }
}

export default Listing;

/* eslint no-undef: "off" */
export const listingQuery = graphql`
query ListingQuery {
  allMarkdownRemark(sort: {fields: fields___date, order: DESC}, limit: 50) {
    edges {
      node {
        fields {
          date(formatString: "DD MMMM, YYYY")
          slug
        }
        excerpt
        timeToRead
        frontmatter {
          title
          tags
          cover {
            childImageSharp {
              fixed(width: 360, quality: 100, toFormat: WEBP) {
                ...GatsbyImageSharpFixed
              }
            }
          }
        }
      }
    }
  }
}

`;
