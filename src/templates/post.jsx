import React from "react";
import Helmet from "react-helmet";
import { graphql } from "gatsby";
import Img from "gatsby-image";
import Layout from "../components/Layout";
import SEO from "../components/SEO/SEO";
import SocialLinks from "../components/SocialLinks";
import Tags from "../components/Tags";
import config from "../../data/SiteConfig";

class PostTemplate extends React.Component {
  render() {
    const { data, pageContext } = this.props;
    const { slug } = pageContext;
    const postNode = data.markdownRemark;
    const post = postNode.frontmatter;
    if (!post.id) {
      post.id = slug;
    }
    if (!post.category_id) {
      post.category_id = config.postDefaultCategoryID;
    }
    return (
      <Layout>
        <Helmet>
          <title>{`${post.title} | ${config.siteTitle}`}</title>
        </Helmet>
        <SEO />
        <div className="columns is-vcentered">
          <div className="column is-6">
            <Img rel="preload" fluid={post.cover.childImageSharp.fluid} alt={post.title} />
          </div>
          <div className="column is-6">
            <div className="description_padding">
              <h1 className="title">
                {post.title}
              </h1>
              <div className="content article-body" dangerouslySetInnerHTML={{ __html: postNode.html }} />
            </div>
            <Tags tags={post.tags} />
            <SocialLinks postPath={slug} postNode={postNode} />
          </div>
        </div>
      </Layout>
    );
  }
}

export default PostTemplate

/* eslint no-undef: "off" */
export const pageQuery = graphql`
query BlogPostBySlug($slug: String!) {
  markdownRemark(fields: {slug: {eq: $slug}}) {
    html
    timeToRead
    excerpt
    frontmatter {
      title
      cover {
        childImageSharp {
          fluid(toFormat: WEBP) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      date
      category
      tags
    }
    fields {
      slug
      date
    }
  }
}
`
