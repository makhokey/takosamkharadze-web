import React, { Component } from "react";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  FacebookShareCount
} from "react-share";
import { SocialIcon } from "react-social-icons";
import urljoin from "url-join";
import config from "../../../data/SiteConfig";

class SocialLinks extends Component {
  render() {
    const { postNode, postPath } = this.props;
    const post = postNode.frontmatter;
    const url = urljoin(config.siteUrl, config.pathPrefix, postPath);
    const filter = count => (count > 0 ? count : "");
    const renderShareCount = count => (
      <div className="share-count">{filter(count)}</div>
    );

    return (
      <div className="social-links">
        <TwitterShareButton url={url} title={post.title}>
          <SocialIcon network="twitter" bgColor="#000000" style={{width:32, height:32}} />
        </TwitterShareButton>
        <FacebookShareButton className="is-post-icon" url={url} quote={postNode.excerpt}>
          <SocialIcon network="facebook" bgColor="#000000" style={{width:32, height:32}} />
          <FacebookShareCount url={url}>
            {count => renderShareCount(count)}
          </FacebookShareCount>
        </FacebookShareButton>
        <LinkedinShareButton
          url={url}
          title={post.title}
          description={postNode.excerpt}
        >
          <SocialIcon network="linkedin" bgColor="#000000" style={{width:32, height:32}} />
        </LinkedinShareButton>
      </div>
    );
  }
}

export default SocialLinks;
