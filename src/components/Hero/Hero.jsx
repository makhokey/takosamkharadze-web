import React, { Component } from "react";
import { injectIntl } from "gatsby-plugin-intl"
import Target from "../../img/target.svg"

class Hero extends Component {
  render() {
    const {intl} = this.props
    return (
      <section className="hero is-fullheight-with-navbar">
        <div className="hero-body">
          <div className="container">
            <div className="columns is-vcentered">
              <div className="column is-1 border-solid">
                <h1 className="title is-size-2">1</h1>
              </div>
              <div className="column is-4 border-solid">
                <h1 className="title is-size-2">1</h1>
              </div>
              <div className="column is-2 border-solid">
                <h1 className="title is-size-2">1</h1>
              </div>
              <div className="column is-4 border-solid">
                <h1 className="title is-size-2">1</h1>
              </div>
              <div className="column is-1 border-solid">
                <h1 className="title is-size-2">1</h1>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default injectIntl(Hero);
