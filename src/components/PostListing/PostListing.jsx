import React from "react";
import AniLink from "gatsby-plugin-transition-link/AniLink";
import Masonry from "react-masonry-component";
import Img from "gatsby-image";

const masonryOptions = {
  transitionDuration: 0,
  itemSelector: ".is-grid-image",
  isFitWidth: true
};

class PostListing extends React.Component {
  getPostList() {
    const postList = [];
    this.props.postEdges.forEach(postEdge => {
      postList.push({
        path: postEdge.node.fields.slug,
        tags: postEdge.node.frontmatter.tags,
        author: postEdge.node.frontmatter.author,
        avatar: postEdge.node.frontmatter.avatar,
        cover: postEdge.node.frontmatter.cover.childImageSharp.fixed,
        title: postEdge.node.frontmatter.title,
        date: postEdge.node.fields.date,
        excerpt: postEdge.node.excerpt,
        timeToRead: postEdge.node.timeToRead
      });
    });
    return postList;
  }


  render() {
    const cat = "";
    const postList = this.getPostList();

    return (
      <Masonry
        className="is-grid"
        elementType="ul"
        options={masonryOptions}
      >
        {
        postList.map(post => (
          <AniLink swipe direction="right" to={cat + post.path} key={post.title}>
            <Img rel="preload" className="is-grid-image" fixed={post.cover} />
          </AniLink>
        ))
      }
      </Masonry>
    );
  }
}

export default PostListing;
