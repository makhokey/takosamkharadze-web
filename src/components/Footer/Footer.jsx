import React from 'react'
import { injectIntl, Link } from "gatsby-plugin-intl"
import { SocialIcon } from "react-social-icons";


const Footer = class extends React.Component {
  render() {
    const {intl} = this.props;
    return (
      <footer className="footer has-text-centered">
        <div className="columns is-multiline">
          <div className="column is-12">
            <div className="footer-logo">
              {intl.formatMessage({ id: 'navbar.title' })}
            </div>
          </div>
          <div className="column is-12 is-centered">
            <SocialIcon network="behance" url="https://www.behance.net/takosamkharadze" target="_blank" bgColor="#000000" style={{width:32, height:32, margin: 5}} />
            <SocialIcon network="dribbble" url="https://dribbble.com/takosamkharadze" target="_blank" bgColor="#000000" style={{width:32, height:32, margin: 5}} />
            <SocialIcon network="instagram" url="https://www.instagram.com/takosamkharadze__" target="_blank" bgColor="#000000" style={{width:32, height:32, margin: 5}} />
          </div>
          <div className="column is-12">
            <Link className="footer-pads" to="/contact">
              {intl.formatMessage({ id: 'footer.contact' })}
            </Link>
            <Link className="footer-pads" to="/about">
              {intl.formatMessage({ id: 'footer.about' })}
            </Link>
          </div>
          <div className="column is-6">
            {intl.formatMessage({ id: 'footer.built' })}
          </div>
          <div className="column is-6">
            {intl.formatMessage({ id: 'footer.copyright' })}
          </div>
        </div>
      </footer>
    )
  }
}

export default injectIntl(Footer)
