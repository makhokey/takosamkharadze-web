import React, { Component } from "react";
import { navigate } from "gatsby-link"
import { injectIntl } from "gatsby-plugin-intl"

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
}

class Start extends Component {
  constructor(props) {
    super(props);
    this.state = { isValidated: false };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const form = e.target;
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...this.state
      })
    })
      .then(() => navigate(form.getAttribute("action")))
      .catch(error => alert(error));
  };

  render() {
    const {intl} = this.props
    return (
      <section className="section">
      <div className="column is-12 column-margin">
    <div className="container has-text-centered">
    <h1 className="title is-1 has-text-black">{intl.formatMessage({ id: 'start.title' })}</h1>
    </div>
      <div className="columns">
      <div className="column is-3"/>
            <div className="column is-6">
  <form
    className="is-medium"
    name="contact"
    method="post"
    action="/thanks"
    data-netlify="true"
    data-netlify-honeypot="bot-field"
    onSubmit={this.handleSubmit}
  >
    {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
    <input type="hidden" name="form-name" value="contact" />
    <div hidden>
      <label>
        Don’t fill this out:{" "}
        <input name="bot-field" onChange={this.handleChange} />
      </label>
    </div>
    <div className="field">
      <div className="control">
        <input className="input is-medium" placeholder={intl.formatMessage({ id: 'start.name' })} type={"text"} name={"name"} onChange={this.handleChange} id={"name"} required={true} />
      </div>
    </div>
    <div className="field">
        <div className="control">
          <input className="input is-medium" placeholder={intl.formatMessage({ id: 'start.email' })} type={"email"} name={"email"} onChange={this.handleChange} id={"email"} required={true} />
        </div>
    </div>
    <div className="field">
        <div className="control">
          <input className="input is-medium" placeholder={intl.formatMessage({ id: 'start.phone' })} type={"tel"} name={"tel"} onChange={this.handleChange} id={"tel"} required={false} />
        </div>
    </div>
    <div className="field">
        <div className="control">
          <input className="input is-medium" placeholder={intl.formatMessage({ id: 'start.find' })} type={"find"} name={"find"} onChange={this.handleChange} id={"find"} required={false} />
        </div>
    </div>
    <div className="field">
      <div className="control">
        <textarea className="textarea is-medium" placeholder={intl.formatMessage({ id: 'start.tell' })} name={"message"} onChange={this.handleChange} id={"message"} required={true} />
      </div>
    </div>
    <div className="field control">
    <button className="button is-primary is-medium is-fullwidth" type="submit">{intl.formatMessage({ id: 'start.talk' })}</button>
    </div>
  </form>
  </div>
  <div className="column is-3" />

  </div>
  </div>
  </section>

    );
  }
}

export default injectIntl(Start);
