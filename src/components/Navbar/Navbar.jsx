import React from 'react'
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl"
import AniLink from "gatsby-plugin-transition-link/AniLink";

const Navbar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      navBarActiveClass: '',
    }
  }

  toggleHamburger = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the navbar accordingly
        this.state.active
          ? this.setState({
              navBarActiveClass: 'is-active',
            })
          : this.setState({
              navBarActiveClass: '',
            })
      }
    )
  }

  render() {
    const intl = this.props.intl;
    return (
      <nav className="navbar" role="navigation" aria-label="main-navigation">
          <div className="container">
              <div className="navbar-brand">
              <AniLink swipe direction="left" className="navbar-logo" to="/">
              {intl.formatMessage({ id: 'navbar.title' })}
              </AniLink>
                  {/* Hamburger menu */}
                  <div className={`navbar-burger burger ${this.state.navBarActiveClass}`} data-target="navMenu" onClick={()=> this.toggleHamburger()}
                      >
                      <span />
                      <span />
                      <span />
                  </div>
              </div>
              <div id="navMenu" className={`navbar-menu ${this.state.navBarActiveClass}`}>
                  <div className="navbar-start">
                  </div>
                  <div className="navbar-end">
                  <AniLink swipe direction="right" className="navbar-item navbar-item-hov" to="/about">
                  {intl.formatMessage({ id: 'navbar.about' })}
                  </AniLink>
                  <AniLink swipe direction="right" className="navbar-item navbar-item-hov" to="/contact">
                  {intl.formatMessage({ id: 'navbar.contact' })}
                  </AniLink>
                  </div>
              </div>
          </div>
      </nav>
    )
  }
}

export default injectIntl(Navbar)
