import React, { Component } from "react";
import { injectIntl } from "gatsby-plugin-intl"
import { navigate } from "gatsby-link"

function encode(data) {
  return Object.keys(data)
    .map(key => `${encodeURIComponent(key)  }=${  encodeURIComponent(data[key])}`)
    .join("&");
}

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = { isValidated: false };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const form = e.target;
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": form.getAttribute("name"),
        ...this.state
      })
    })
      .then(() => navigate(form.getAttribute("action")))
      .catch(error => alert(error));
  };

  render() {
    const {intl} = this.props
    return (
      <section className="hero is-fullheight-with-navbar">
        <div className="hero-body">
          <div className="column">
          <h1 className="title is-2 has-text-centered">{intl.formatMessage({ id: 'contact.what' })}</h1>
            <div className="columns is-centered">
              <div className="column is-6">
              <form
                className="is-medium"
                name="Contact"
                method="post"
                action="/thanks"
                data-netlify="true"
                data-netlify-honeypot="bot-field"
                onSubmit={this.handleSubmit}
              >
              <input type="hidden" name="form-name" value="Contact" />
              <div hidden>
                <label>
                  <input name="bot-field" onChange={this.handleChange} />
                </label>
              </div>
                  <div className="field control">
                      <input className="input" type="name" placeholder={intl.formatMessage({ id: 'contact.name' })} type={"text"} name={"name"} onChange={this.handleChange} id={"name"} required={true} />
                  </div>
                  <div className="field control">
                      <input className="input" type="email" placeholder={intl.formatMessage({ id: 'contact.email' })} type={"email"} name={"email"} onChange={this.handleChange} id={"email"} required={true} />
                  </div>
                  <div className="field control">
                      <textarea className="textarea" rows="3" placeholder={intl.formatMessage({ id: 'contact.textarea' })} type={"text"} name={"field"} onChange={this.handleChange} id={"field"} required={false} />
                  </div>
                  <div className="field control">
                      <button className="button is-primary is-fullwidth" type="submit">{intl.formatMessage({ id: 'contact.submit' })}</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default injectIntl(Contact);
