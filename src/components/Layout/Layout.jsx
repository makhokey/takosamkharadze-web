import React from 'react'
import { Helmet } from 'react-helmet'
import { injectIntl } from "gatsby-plugin-intl"
import Footer from '../Footer'
import Navbar from '../Navbar'
import config from "../../../data/SiteConfig"
import '../styles.sass'

class MainLayout extends React.Component {
  render() {
    const { children } = this.props;
    return (
      <div>
        <Navbar />
        <Helmet>
          <meta name="description" content={config.siteDescription} />
          <html lang="en" />
        </Helmet>
        <div className="section">
          {children}
        </div>
        <Footer />
      </div>
    );
  }
}

export default injectIntl(MainLayout);
