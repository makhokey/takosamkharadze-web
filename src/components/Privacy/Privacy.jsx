import React, { Component } from "react";
import { injectIntl } from "gatsby-plugin-intl"

class Privacy extends Component {
  render() {
    const {intl} = this.props
    return (
      <section className="section">
        <div className="column is-12 column-margin">
          <div className="container has-text-centered">
            <h1 className="title is-1 has-text-black">{intl.formatMessage({ id: 'privacy.title' })}</h1>
          </div>
        </div>
      </section>
    );
  }
}

export default injectIntl(Privacy);
