import React, { Component } from "react";
import { injectIntl } from "gatsby-plugin-intl"

class About extends Component {
  render() {
    const {intl} = this.props;
    return (
      <section className="hero is-fullheight-with-navbar">
        <div className="hero-body">
          <div className="column">
            <h1 className="title is-2 has-text-left">{intl.formatMessage({ id: 'about.title' })}</h1>
            <h2 className="subtitle is-4 is-black has-text-left">{intl.formatMessage({ id: 'about.hi' })}</h2>
          </div>
        </div>
      </section>
    );
  }
}

export default injectIntl(About);
