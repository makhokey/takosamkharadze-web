---
title: Baby Anna
date: 2020-02-11T12:42:49.793Z
cover: /assets/anna-portrait.jpg
slug: baby-anna
category: Portraits
tags:
  - Portraits
  - Digital Illustration
---
Portrait of the baby Anna! 🍓
