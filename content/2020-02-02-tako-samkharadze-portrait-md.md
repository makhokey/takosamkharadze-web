---
title: Tako Samkharadze Portrait
date: 2020-02-02T11:02:12.579Z
cover: /assets/tako-self-portrait.jpg
slug: tako-samkharadze-portrait
category: Portrait
tags:
  - portrait
  - digital
---
A digital self-portrait by an illustrator.
